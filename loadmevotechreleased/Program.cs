﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using ExcelDataReader;
using ExcelDataReader.Core;


namespace loadmevotechreleased
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {

                string inputfilefolder = ConfigurationManager.AppSettings["InputfileFolder"];
                string ArchiveFolder = ConfigurationManager.AppSettings["ArchiveFolder"];
                string CancellationFolder = ConfigurationManager.AppSettings["CancellationFolder"];
                string CancellationArchiveFolder = ConfigurationManager.AppSettings["CancellationArchiveFolder"];
                
                #region Mevotech Cancellation Shipment
                string[] cancelfiles = Directory.GetFiles(CancellationFolder, "*cancel*.csv", SearchOption.TopDirectoryOnly);


                foreach (string filename in cancelfiles)
                {
                    string targetFilename = CancellationArchiveFolder + "\\" + Path.GetFileName(filename);


                    File.Copy(filename, targetFilename, true);

                    Console.WriteLine("Reading file {0}", filename);

                    if (Path.GetExtension(filename) == ".csv" && Path.GetFileName(filename).IndexOf("-cancel") > 0)
                    {
                        string line;
                        StreamReader sr = new StreamReader(filename);
                        
                        int idx = 0;

                        SqlConnection cnCancel = new SqlConnection();
                        SqlCommand cmdCancel = new SqlCommand();
                        cnCancel.ConnectionString = ConfigurationManager.ConnectionStrings["AzureAPP"].ConnectionString;


                        cnCancel.Open();
                        cmdCancel.Connection = cnCancel;
                        cmdCancel.CommandType = CommandType.Text;
                        



                        while ((line = sr.ReadLine()) != null)
                        {
                            string[] arrLine = null;


                            if (Path.GetExtension(filename) == ".csv")
                            {
                                arrLine = line.Split(',');

                                if (arrLine.Length == 4)
                                {
                                    cmdCancel.CommandText = "if not exists (select * from MEVO.ExcludeTransaction where TransactionNumber = '" + arrLine[0].Replace("\"","").Trim() + 
                                        "') insert MEVO.ExcludeTransaction (OmniReferenceNumber, TransactionNumber, CancelledDate, CancelledReason) values ('From Mevo ftp', '" 
                                        + arrLine[0].Replace("\"", "").Trim() + "','" + arrLine[2].Replace("\"", "") + "','" + arrLine[3].Replace("\"", "") + "')";

                                    cmdCancel.ExecuteNonQuery();

                                    
                                }
                            }



                            idx++;
                        }

                        sr.Dispose();
                        File.Delete(filename);

                        cnCancel.Close();
                        cnCancel.Dispose();
                        Console.WriteLine(idx.ToString() + " rows loaded");

                        SendEmail("New Mevo Cancel file is loaded:" + Path.GetFileName(filename), "New Mevo Cancel file is loaded:" + Path.GetFileName(filename));
                    }
                 }

                #endregion

                    string[] files = Directory.GetFiles(inputfilefolder, "Mevotech_Released*.txt", SearchOption.TopDirectoryOnly);

                if  (files.Length == 0)
                {
                    files = Directory.GetFiles(inputfilefolder, "Mevotech_Released*.csv", SearchOption.TopDirectoryOnly);
                }

                if (files.Length == 0)
                {
                    files = Directory.GetFiles(inputfilefolder, "Mevotech Released*.xlsx", SearchOption.TopDirectoryOnly);
                }


                SqlConnection cn = new SqlConnection();
                SqlCommand cmd = new SqlCommand();
                cn.ConnectionString = ConfigurationManager.ConnectionStrings["AzureAPP"].ConnectionString;


                cn.Open();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "";

                SqlBulkCopy bulkcopy = new SqlBulkCopy(cn);
                bulkcopy.BulkCopyTimeout = 9999;
                bulkcopy.DestinationTableName = "[MEVO].[MevotechReleased]";




                #region Mevotech_Released files

                foreach (string filename in files)
                {

                    //copy files to Archive folder \\mtlvfilesvr1.omni.local\e\DataTransfer\InboundFTP\Smartborder\Report\Archive
                    File.Copy(filename, filename.Replace("Report", "Report\\Archive"),true);


                    Console.WriteLine("Reading file {0}", filename);

                    if (Path.GetExtension(filename) == ".xlsx")
                    {
                        FileStream fStream = File.Open(filename, FileMode.Open, FileAccess.Read);
                        IExcelDataReader excelDataReader = ExcelReaderFactory.CreateOpenXmlReader(fStream);
                        DataSet resultDataSet = excelDataReader.AsDataSet();
                        excelDataReader.Close();

                        DataTable dtExcel = resultDataSet.Tables[0];

                        dtExcel.Rows[0].Delete();
                        dtExcel.AcceptChanges();
                        dtExcel.Columns.Add("Filename", typeof(string));

                        string excelFilename = Path.GetFileName(filename);
                        foreach (DataRow dr in dtExcel.Rows)
                        {
                            dr["Filename"] = excelFilename;
                        }

                        

                        bulkcopy.WriteToServer(dtExcel);

                        Console.WriteLine(dtExcel.Rows.Count.ToString() + " rows loaded");

                        File.Delete(filename);

                    }
                    else
                    {
                        string line;
                        StreamReader sr = new StreamReader(filename);
                        DataTable dt = new DataTable();
                        DataRow dr;
                        int idx = 0;
                        while ((line = sr.ReadLine()) != null)
                        {
                            string[] arrLine = null;

                            if (Path.GetExtension(filename) == ".txt")
                            {
                                arrLine = line.Split('\t');
                            }
                            else if (Path.GetExtension(filename) == ".csv")
                            {
                                arrLine = line.Split(',');
                            }



                            if (arrLine.Length == 3)
                            {
                                if (idx == 0)
                                {
                                    foreach (string columnname in arrLine)
                                    {
                                        dt.Columns.Add(columnname.Replace("\"", "").Trim(), typeof(string));
                                    }

                                    dt.Columns.Add("Filename", typeof(string));
                                    dt.Columns["Filename"].DefaultValue = Path.GetFileName(filename);
                                }
                                else
                                {


                                    dr = dt.NewRow();

                                    for (int i = 0; i < arrLine.Length; i++)
                                    {
                                        dr[i] = arrLine[i].Replace("\"", "").Trim();
                                    }

                                    dt.Rows.Add(dr);

                                }
                            }
                            idx++;
                        }
                        //bulkcopy.ColumnMappings.Add("Filename", "Filename");
                        bulkcopy.WriteToServer(dt);

                        Console.WriteLine(idx.ToString() + " rows loaded");
                    }
                }

                #endregion


                #region Mevotech_Modified files

                
                files = Directory.GetFiles(inputfilefolder, "Mevotech_Modified*.txt", SearchOption.TopDirectoryOnly);

                if (files.Length == 0)
                {
                    files = Directory.GetFiles(inputfilefolder, "Mevotech_Modified*.csv", SearchOption.TopDirectoryOnly);
                }

                if (files.Length == 0)
                {
                    files = Directory.GetFiles(inputfilefolder, "Mevotech Modified*.xlsx", SearchOption.TopDirectoryOnly);
                }


                bulkcopy.DestinationTableName = "[MEVO].[MevotechModified]";

                foreach (string filename in files)
                {

                    //copy files to Archive folder \\mtlvfilesvr1.omni.local\e\DataTransfer\InboundFTP\Smartborder\Report\Archive
                    File.Copy(filename, filename.Replace("Report", "Report\\Archive"), true);

                    Console.WriteLine("Reading file {0}", filename);

                    if (Path.GetExtension(filename) == ".xlsx")
                    {
                        FileStream fStream = File.Open(filename, FileMode.Open, FileAccess.Read);
                        IExcelDataReader excelDataReader = ExcelReaderFactory.CreateOpenXmlReader(fStream);
                        DataSet resultDataSet = excelDataReader.AsDataSet();
                        excelDataReader.Close();

                        DataTable dtExcel = resultDataSet.Tables[0];
                        dtExcel.Columns.Add("Filename", typeof(string));
                        dtExcel.Rows[0].Delete();
                        dtExcel.AcceptChanges();

                        string excelFilename = Path.GetFileName(filename);
                        foreach (DataRow dr in dtExcel.Rows)
                        {
                            dr["Filename"] = excelFilename;
                        }


                        bulkcopy.WriteToServer(dtExcel);

                        Console.WriteLine(dtExcel.Rows.Count.ToString() + " rows loaded");

                        File.Delete(filename);
                    }
                    else
                    {

                        string line;
                        StreamReader sr = new StreamReader(filename);
                        DataTable dt = new DataTable();
                        DataRow dr;
                        int idx = 0;
                        while ((line = sr.ReadLine()) != null)
                        {
                            string[] arrLine = null;

                            if (Path.GetExtension(filename) == ".txt")
                            {
                                arrLine = line.Split('\t');
                            }
                            else if (Path.GetExtension(filename) == ".csv")
                            {
                                arrLine = line.Split(',');
                            }

                            if (arrLine.Length == 10)
                            {
                                if (idx == 0)
                                {
                                    foreach (string columnname in arrLine)
                                    {
                                        dt.Columns.Add(columnname.Replace("\"", "").Trim(), typeof(string));
                                    }

                                    dt.Columns.Add("Filename", typeof(string));
                                    dt.Columns["Filename"].DefaultValue = Path.GetFileName(filename);
                                }
                                else
                                {


                                    dr = dt.NewRow();

                                    for (int i = 0; i < arrLine.Length; i++)
                                    {
                                        dr[i] = arrLine[i].Replace("\"", "").Trim();
                                    }

                                    dt.Rows.Add(dr);

                                }
                            }
                            idx++;
                        }


                        //bulkcopy.ColumnMappings.Add("Filename", "Filename");
                        bulkcopy.WriteToServer(dt);

                        Console.WriteLine(idx.ToString() + " rows loaded");
                    }

                }

                #endregion


                #region Mevo Transaction update files


                files = Directory.GetFiles(inputfilefolder, "Mevotech_update*.txt", SearchOption.TopDirectoryOnly);

                if (files.Length == 0)
                {
                    files = Directory.GetFiles(inputfilefolder, "Mevotech_update*.csv", SearchOption.TopDirectoryOnly);
                }

                foreach (string filename in files)
                {

                    Console.WriteLine("Reading file {0}", filename);
                    string line;
                    StreamReader sr = new StreamReader(filename);
                    DataTable dt = new DataTable();
                    DataRow dr;
                    int idx = 0;
                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] arrLine = null;

                        if (Path.GetExtension(filename) == ".txt")
                        {
                            arrLine = line.Split('\t');
                        }
                        else if (Path.GetExtension(filename) == ".csv")
                        {
                            arrLine = line.Split(',');
                        }

                        if (arrLine.Length == 2)
                        {
                            if (idx == 0)
                            {
                                foreach (string columnname in arrLine)
                                {
                                    dt.Columns.Add(columnname.Replace("\"", "").Trim(), typeof(string));
                                }

                                dt.Columns.Add("Filename", typeof(string));
                                dt.Columns["Filename"].DefaultValue = Path.GetFileName(filename);
                            }
                            else
                            {


                                dr = dt.NewRow();

                                for (int i = 0; i < arrLine.Length; i++)
                                {
                                    dr[i] = arrLine[i].Replace("\"", "").Trim();
                                }

                                dt.Rows.Add(dr);

                            }
                        }
                        idx++;
                    }

                    
                    bulkcopy.BulkCopyTimeout = 9999;
                    bulkcopy.DestinationTableName = "[MEVO].[MevotechTransactionUpdate]";
                    //bulkcopy.ColumnMappings.Add("Filename", "Filename");
                    bulkcopy.WriteToServer(dt);

                    Console.WriteLine(idx.ToString() + " rows loaded");

                }

                #endregion

                cn.Close();
                cn.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        static void SendEmail(string Subject, string EmailBody)
        {
            MailMessage message = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            message.From = new MailAddress("elee@omnitrans.com");
            message.To.Add(new MailAddress("elee@omnitrans.com"));
            message.Subject = Subject;
            message.IsBodyHtml = false; //to make message body as html  
            message.Body = EmailBody;
            smtp.Port = 587;
            smtp.Host = "SMTP.SendGrid.net"; //for gmail host  
                                             //smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("apikey", "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");
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Send(message);
        }

    }
}
